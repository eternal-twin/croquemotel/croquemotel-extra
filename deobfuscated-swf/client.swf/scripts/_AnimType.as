package
{
   import flash.Boot;
   
   public final class _AnimType
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["Explode","Q\x01Z^\x03","\x12\x1c\x04\x1e\x03","d;,*\x02","en\x01^","o;T\\\x02","rU+!",";^@n\x02"];
      
      public static var Explode:_AnimType;
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function _AnimType(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public static function §en\x01^§(param1:int) : _AnimType
      {
         return new _AnimType("en\x01^",4,[param1]);
      }
      
      public static function §rU+!§(param1:int) : _AnimType
      {
         return new _AnimType("rU+!",6,[param1]);
      }
      
      public static function §d;,*\x02§(param1:int) : _AnimType
      {
         return new _AnimType("d;,*\x02",3,[param1]);
      }
      
      public static function §\x12\x1c\x04\x1e\x03§(param1:int, param2:int) : _AnimType
      {
         return new _AnimType("\x12\x1c\x04\x1e\x03",2,[param1,param2]);
      }
      
      public static function §Q\x01Z^\x03§(param1:int) : _AnimType
      {
         return new _AnimType("Q\x01Z^\x03",1,[param1]);
      }
      
      public static function §;^@n\x02§(param1:int) : _AnimType
      {
         return new _AnimType(";^@n\x02",7,[param1]);
      }
      
      public static function §o;T\\\x02§(param1:int) : _AnimType
      {
         return new _AnimType("o;T\\\x02",5,[param1]);
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
