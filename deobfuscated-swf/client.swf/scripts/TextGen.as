package
{
   import flash.Boot;
   import haxe.Resource;
   import mt.Rand;
   
   public class TextGen
   {
      
      public static var §?}\f$\x02§:Hash;
       
      
      public var §T6ka\x01§:Rand;
      
      public function TextGen(param1:int = 0)
      {
         var _loc3_:* = null as Array;
         var _loc4_:* = null as String;
         var _loc5_:int = 0;
         var _loc6_:* = null as String;
         var _loc7_:* = null as String;
         if(Boot.skip_constructor)
         {
            return;
         }
         var _loc2_:String = Resource.getString(Game.LANG + ".texts.xml");
         if(_loc2_ == null || _loc2_ == "")
         {
            TextGen.fatal("no data");
         }
         §T6ka\x01§ = new Rand(0);
         §T6ka\x01§.initSeed(param1);
         if(TextGen.§?}\f$\x02§ == null)
         {
            TextGen.§?}\f$\x02§ = new Hash();
            _loc3_ = _loc2_.split("\n");
            _loc4_ = null;
            _loc5_ = 0;
            while(_loc5_ < int(_loc3_.length))
            {
               _loc6_ = _loc3_[_loc5_];
               _loc5_++;
               _loc7_ = StringTools.trim(_loc6_);
               if(_loc7_.length != 0)
               {
                  if(_loc6_.charAt(0) == " ")
                  {
                     TextGen.fatal("unexpected leading space around key " + _loc4_);
                  }
                  if(_loc6_.charAt(0) != "j")
                  {
                     _loc4_ = _loc7_.toLowerCase();
                  }
                  else
                  {
                     if(_loc4_ == null)
                     {
                        TextGen.fatal("unexpected : " + _loc6_);
                     }
                     if(TextGen.§?}\f$\x02§.get(_loc4_) == null)
                     {
                        TextGen.§?}\f$\x02§.set(_loc4_,[]);
                     }
                     TextGen.§?}\f$\x02§.get(_loc4_).push(_loc7_);
                  }
               }
            }
         }
      }
      
      public static function fatal(param1:String) : void
      {
         Boot.lastError = new Error();
         throw "TextGen : " + param1;
      }
      
      public function initSeed(param1:int) : void
      {
         §T6ka\x01§ = new Rand(0);
         §T6ka\x01§.initSeed(param1);
      }
      
      public function get(param1:String, param2:Object = undefined, param3:Object = undefined) : String
      {
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:* = null as String;
         if(param3 == null)
         {
            param3 = true;
         }
         if(param2 == null)
         {
            param2 = §T6ka\x01§.random;
         }
         param1 = param1.toLowerCase();
         var _loc4_:Array = TextGen.§?}\f$\x02§.get(param1);
         if(param1 == null || _loc4_ == null || int(_loc4_.length) == 0)
         {
            TextGen.fatal("unknown key " + param1);
         }
         var _loc5_:String = _loc4_[int(param2(int(_loc4_.length)))];
         var _loc6_:Array = _loc5_.split("L");
         if(int(_loc6_.length) > 1)
         {
            _loc5_ = "";
            _loc7_ = 1;
            _loc8_ = 0;
            while(_loc8_ < int(_loc6_.length))
            {
               _loc9_ = _loc6_[_loc8_];
               _loc8_++;
               if(int(_loc7_ % 2) == 0)
               {
                  _loc5_ = _loc5_ + get(_loc9_,null,false);
               }
               else
               {
                  _loc5_ = _loc5_ + _loc9_;
               }
               _loc7_++;
            }
         }
         return _loc5_;
      }
      
      public function format(param1:String, param2:*) : String
      {
         var _loc8_:* = null as String;
         var _loc3_:String = get(param1);
         var _loc4_:Array = _loc3_.split("::");
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:Array = Reflect.fields(param2);
         while(_loc6_ < int(_loc7_.length))
         {
            _loc8_ = _loc7_[_loc6_];
            _loc6_++;
            _loc3_ = StringTools.replace(_loc3_,"::" + _loc8_.substr(1) + "::",Reflect.field(param2,_loc8_));
         }
         return _loc3_;
      }
   }
}
