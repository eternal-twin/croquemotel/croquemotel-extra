package
{
   import flash.Boot;
   
   public class List
   {
       
      
      public var mg:Array;
      
      public var length:int;
      
      public var P:Array;
      
      public function List()
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         length = 0;
      }
      
      public function toString() : String
      {
         var _loc1_:StringBuf = new StringBuf();
         var _loc2_:Boolean = true;
         var _loc3_:Array = P;
         _loc1_.b = _loc1_.b + "H";
         while(_loc3_ != null)
         {
            if(_loc2_)
            {
               _loc2_ = false;
            }
            else
            {
               _loc1_.b = _loc1_.b + ", ";
            }
            _loc1_.b = _loc1_.b + Std.string(Std.string(_loc3_[0]));
            _loc3_ = _loc3_[1];
         }
         _loc1_.b = _loc1_.b + "}";
         return _loc1_.b;
      }
      
      public function remove(param1:Object) : Boolean
      {
         var _loc2_:Array = null;
         var _loc3_:Array = P;
         while(_loc3_ != null)
         {
            if(_loc3_[0] == param1)
            {
               if(_loc2_ == null)
               {
                  P = _loc3_[1];
               }
               else
               {
                  _loc2_[1] = _loc3_[1];
               }
               if(mg == _loc3_)
               {
                  mg = _loc2_;
               }
               length = length - 1;
               return true;
            }
            _loc2_ = _loc3_;
            _loc3_ = _loc3_[1];
         }
         return false;
      }
      
      public function push(param1:Object) : void
      {
         var _loc2_:Array = [param1,P];
         P = _loc2_;
         if(mg == null)
         {
            mg = _loc2_;
         }
         length = length + 1;
      }
      
      public function pop() : Object
      {
         if(P == null)
         {
            return null;
         }
         var _loc1_:Object = P[0];
         P = P[1];
         if(P == null)
         {
            mg = null;
         }
         length = length - 1;
         return _loc1_;
      }
      
      public function map(param1:Function) : List
      {
         var _loc4_:* = null as Object;
         var _loc2_:List = new List();
         var _loc3_:Array = P;
         while(_loc3_ != null)
         {
            _loc4_ = _loc3_[0];
            _loc3_ = _loc3_[1];
            _loc2_.add(param1(_loc4_));
         }
         return _loc2_;
      }
      
      public function last() : Object
      {
         return mg == null ? null : mg[0];
      }
      
      public function join(param1:String) : String
      {
         var _loc2_:StringBuf = new StringBuf();
         var _loc3_:Boolean = true;
         var _loc4_:Array = P;
         while(_loc4_ != null)
         {
            if(_loc3_)
            {
               _loc3_ = false;
            }
            else
            {
               _loc2_.b = _loc2_.b + Std.string(param1);
            }
            _loc2_.b = _loc2_.b + Std.string(_loc4_[0]);
            _loc4_ = _loc4_[1];
         }
         return _loc2_.b;
      }
      
      public function iterator() : Object
      {
         return {
            "P":P,
            "\n\x1cT[\x02":function():*
            {
               return this.P != null;
            },
            "next":function():*
            {
               if(this.P == null)
               {
                  return null;
               }
               var _loc1_:* = this.P[0];
               this.P = this.P[1];
               return _loc1_;
            }
         };
      }
      
      public function isEmpty() : Boolean
      {
         return P == null;
      }
      
      public function first() : Object
      {
         return P == null ? null : P[0];
      }
      
      public function filter(param1:Function) : List
      {
         var _loc4_:* = null as Object;
         var _loc2_:List = new List();
         var _loc3_:Array = P;
         while(_loc3_ != null)
         {
            _loc4_ = _loc3_[0];
            _loc3_ = _loc3_[1];
            if(param1(_loc4_))
            {
               _loc2_.add(_loc4_);
            }
         }
         return _loc2_;
      }
      
      public function clear() : void
      {
         P = null;
         mg = null;
         length = 0;
      }
      
      public function add(param1:Object) : void
      {
         var _loc2_:Array = [param1];
         if(P == null)
         {
            P = _loc2_;
         }
         else
         {
            mg[1] = _loc2_;
         }
         mg = _loc2_;
         length = length + 1;
      }
   }
}
