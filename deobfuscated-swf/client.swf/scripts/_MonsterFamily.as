package
{
   import flash.Boot;
   
   public final class _MonsterFamily
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["_MF_FIRE","_MF_AQUA","_MF_BLOB","_MF_GHOST","_MF_SM","_MF_BOMB","_MF_VEGETAL","_MF_BUSINESS","_MF_FRANK","_MF_GIFT","_MF_BASIC","_MF_ZOMBIE","_MF_FLYING"];
      
      public static var _MF_ZOMBIE:_MonsterFamily;
      
      public static var _MF_VEGETAL:_MonsterFamily;
      
      public static var _MF_SM:_MonsterFamily;
      
      public static var _MF_GIFT:_MonsterFamily;
      
      public static var _MF_GHOST:_MonsterFamily;
      
      public static var _MF_FRANK:_MonsterFamily;
      
      public static var _MF_FLYING:_MonsterFamily;
      
      public static var _MF_FIRE:_MonsterFamily;
      
      public static var _MF_BUSINESS:_MonsterFamily;
      
      public static var _MF_BOMB:_MonsterFamily;
      
      public static var _MF_BLOB:_MonsterFamily;
      
      public static var _MF_BASIC:_MonsterFamily;
      
      public static var _MF_AQUA:_MonsterFamily;
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function _MonsterFamily(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
