package
{
   import flash.Boot;
   
   public final class _TypeRoom
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["_TR_VOID","_TR_BEDROOM","_TR_NONE","_TR_LOBBY","_TR_POOL","_TR_RESTAURANT","_TR_FURNACE","_TR_BIN","_TR_DISCO","_TR_SERV_WASH","_TR_SERV_SHOE","_TR_SERV_FRIDGE","_TR_SERV_ALCOOL","_TR_LAB"];
      
      public static var _TR_VOID:_TypeRoom;
      
      public static var _TR_SERV_WASH:_TypeRoom;
      
      public static var _TR_SERV_SHOE:_TypeRoom;
      
      public static var _TR_SERV_FRIDGE:_TypeRoom;
      
      public static var _TR_SERV_ALCOOL:_TypeRoom;
      
      public static var _TR_RESTAURANT:_TypeRoom;
      
      public static var _TR_POOL:_TypeRoom;
      
      public static var _TR_NONE:_TypeRoom;
      
      public static var _TR_LOBBY:_TypeRoom;
      
      public static var _TR_LAB:_TypeRoom;
      
      public static var _TR_FURNACE:_TypeRoom;
      
      public static var _TR_DISCO:_TypeRoom;
      
      public static var _TR_BIN:_TypeRoom;
      
      public static var _TR_BEDROOM:_TypeRoom;
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function _TypeRoom(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
