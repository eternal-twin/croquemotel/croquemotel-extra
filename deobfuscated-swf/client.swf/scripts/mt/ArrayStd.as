package mt
{
   public class ArrayStd
   {
       
      
      public function ArrayStd()
      {
      }
      
      public static function size(param1:Array) : int
      {
         return int(param1.length);
      }
      
      public static function first(param1:Array) : Object
      {
         return param1[0];
      }
      
      public static function last(param1:Array) : Object
      {
         return param1[int(param1.length) - 1];
      }
      
      public static function clear(param1:Array) : Array
      {
         param1.splice(0,int(param1.length));
         return param1;
      }
      
      public static function set(param1:Array, param2:int, param3:Object) : Array
      {
         param1[param2] = param3;
         return param1;
      }
      
      public static function at(param1:Array, param2:int) : Object
      {
         return param1[param2];
      }
      
      public static function indexOf(param1:Array, param2:Object) : int
      {
         var _loc6_:* = null as Object;
         var _loc3_:int = -1;
         var _loc4_:int = -1;
         var _loc5_:int = 0;
         while(_loc5_ < int(param1.length))
         {
            _loc6_ = param1[_loc5_];
            _loc5_++;
            _loc4_++;
            if(_loc6_ == param2)
            {
               _loc3_ = _loc4_;
               break;
            }
         }
         return _loc3_;
      }
      
      public static function addFirst(param1:Array, param2:Object) : Array
      {
         param1.unshift(param2);
         return param1;
      }
      
      public static function addLast(param1:Array, param2:Object) : Array
      {
         param1.push(param2);
         return param1;
      }
      
      public static function removeFirst(param1:Array) : Object
      {
         return param1.shift();
      }
      
      public static function removeLast(param1:Array) : Object
      {
         return param1.pop();
      }
      
      public static function map(param1:Array, param2:Function) : Array
      {
         var _loc5_:* = null as Object;
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         while(_loc4_ < int(param1.length))
         {
            _loc5_ = param1[_loc4_];
            _loc4_++;
            _loc3_.push(param2(_loc5_));
         }
         return _loc3_;
      }
      
      public static function stripNull(param1:Array) : Array
      {
         while(param1.remove(null))
         {
         }
         return param1;
      }
      
      public static function flatten(param1:Array) : Array
      {
         var _loc5_:int = 0;
         var _loc6_:* = null;
         var _loc7_:* = null as Object;
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         var _loc4_:int = int(param1.length);
         while(_loc3_ < _loc4_)
         {
            _loc5_ = _loc3_++;
            _loc6_ = param1[_loc5_].iterator();
            while(_loc6_.§\n\x1cT[\x02§())
            {
               _loc7_ = _loc6_.next();
               _loc2_.push(_loc7_);
               _loc2_;
            }
            _loc2_;
         }
         return _loc2_;
      }
      
      public static function append(param1:Array, param2:Object) : Array
      {
         var _loc4_:* = null as Object;
         var _loc3_:* = param2.iterator();
         while(_loc3_.§\n\x1cT[\x02§())
         {
            _loc4_ = _loc3_.next();
            param1.push(_loc4_);
            param1;
         }
         return param1;
      }
      
      public static function prepend(param1:Array, param2:Object) : Array
      {
         var _loc5_:* = null as Object;
         var _loc3_:Array = Lambda.array(param2);
         _loc3_.reverse();
         var _loc4_:int = 0;
         while(_loc4_ < int(_loc3_.length))
         {
            _loc5_ = _loc3_[_loc4_];
            _loc4_++;
            param1.unshift(_loc5_);
            param1;
         }
         return param1;
      }
      
      public static function §7\x03\b\n§(param1:Array, param2:Object = undefined) : Array
      {
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:* = null as Object;
         var _loc3_:* = param2 != null ? param2 : Std.random;
         var _loc4_:int = int(param1.length);
         var _loc5_:int = 0;
         var _loc6_:int = _loc4_ << 1;
         while(_loc5_ < _loc6_)
         {
            _loc7_ = _loc5_++;
            _loc8_ = int(_loc3_(_loc4_));
            _loc9_ = int(_loc3_(_loc4_));
            _loc10_ = param1[_loc8_];
            param1[_loc8_] = param1[_loc9_];
            param1[_loc9_] = _loc10_;
         }
         return param1;
      }
      
      public static function getRandom(param1:Array, param2:Object = undefined) : Object
      {
         var _loc3_:* = param2 != null ? param2 : Std.random;
         var _loc4_:int = int(_loc3_(int(param1.length)));
         return param1[_loc4_];
      }
      
      public static function §s}\nE§(param1:Array, param2:Function) : Array
      {
         var _loc6_:Boolean = false;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:* = null as Object;
         var _loc3_:Array = param1;
         var _loc4_:int = 0;
         var _loc5_:int = int(param1.length);
         while(_loc4_ < _loc5_)
         {
            _loc6_ = false;
            _loc7_ = 0;
            _loc8_ = _loc5_ - _loc4_ - 1;
            while(_loc7_ < _loc8_)
            {
               if(int(param2(_loc3_[_loc7_],_loc3_[_loc7_ + 1])) > 0)
               {
                  _loc9_ = _loc3_[_loc7_ + 1];
                  _loc3_[_loc7_ + 1] = _loc3_[_loc7_];
                  _loc3_[_loc7_] = _loc9_;
                  _loc6_ = true;
               }
               _loc7_ = _loc7_ + 1;
            }
            if(!_loc6_)
            {
               break;
            }
            _loc4_ = _loc4_ + 1;
         }
         return _loc3_;
      }
   }
}
