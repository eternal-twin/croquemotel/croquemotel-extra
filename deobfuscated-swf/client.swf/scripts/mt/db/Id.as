package mt.db
{
   import flash.Boot;
   
   public class Id
   {
       
      
      public function Id()
      {
      }
      
      public static function §Mwd\x02§(param1:String) : int
      {
         var _loc5_:int = 0;
         var _loc2_:int = param1.length;
         if(_loc2_ > 6)
         {
            Boot.lastError = new Error();
            throw "Invalid identifier \'" + param1 + "N";
         }
         var _loc3_:int = 0;
         var _loc4_:int = _loc2_;
         for(; _loc4_ > 0; _loc3_ = _loc3_ << 5,_loc3_ = _loc3_ + _loc5_)
         {
            _loc4_--;
            _loc5_ = param1.charCodeAt(_loc4_) - 96;
            if(_loc5_ < 1 || _loc5_ > 26)
            {
               _loc5_ = _loc5_ + 96 - 48;
               if(_loc5_ >= 1 && _loc5_ <= 5)
               {
                  _loc5_ = _loc5_ + 26;
                  continue;
               }
               Boot.lastError = new Error();
               throw "Invalid character " + param1.charCodeAt(_loc4_) + " in " + param1;
            }
         }
         return _loc3_;
      }
      
      public static function decode(param1:int) : String
      {
         var _loc3_:int = 0;
         var _loc2_:StringBuf = new StringBuf();
         if(param1 < 1)
         {
            if(param1 == 0)
            {
               return "";
            }
            Boot.lastError = new Error();
            throw "Invalid ID " + param1;
         }
         while(param1 > 0)
         {
            _loc3_ = param1 & 31;
            if(_loc3_ < 27)
            {
               _loc2_.b = _loc2_.b + String.fromCharCode(_loc3_ + 96);
            }
            else
            {
               _loc2_.b = _loc2_.b + String.fromCharCode(_loc3_ + 22);
            }
            param1 = param1 >> 5;
         }
         return _loc2_.b;
      }
   }
}
