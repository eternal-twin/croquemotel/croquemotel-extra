package mt
{
   public class ListStd
   {
       
      
      public function ListStd()
      {
      }
      
      public static function size(param1:List) : int
      {
         return param1.length;
      }
      
      public static function at(param1:List, param2:int) : Object
      {
         var _loc3_:* = param1.iterator();
         while(true)
         {
            param2--;
            if(param2 <= -1)
            {
               break;
            }
            _loc3_.next();
         }
         return _loc3_.next();
      }
      
      public static function indexOf(param1:List, param2:Object) : int
      {
         var _loc6_:* = null as Object;
         var _loc3_:int = -1;
         var _loc4_:int = -1;
         var _loc5_:* = param1.iterator();
         while(_loc5_.§\n\x1cT[\x02§())
         {
            _loc6_ = _loc5_.next();
            _loc4_++;
            if(_loc6_ == param2)
            {
               _loc3_ = _loc4_;
               break;
            }
         }
         return _loc3_;
      }
      
      public static function addFirst(param1:List, param2:Object) : List
      {
         param1.push(param2);
         return param1;
      }
      
      public static function addLast(param1:List, param2:Object) : List
      {
         param1.add(param2);
         return param1;
      }
      
      public static function removeFirst(param1:List) : Object
      {
         return param1.pop();
      }
      
      public static function removeLast(param1:List) : Object
      {
         var _loc7_:int = 0;
         var _loc2_:List = Lambda.list(param1);
         var _loc3_:* = _loc2_.iterator();
         var _loc4_:Object = param1.last();
         param1.clear();
         var _loc5_:int = 0;
         var _loc6_:int = _loc2_.length - 1;
         while(_loc5_ < _loc6_)
         {
            _loc7_ = _loc5_++;
            param1.add(_loc3_.next());
         }
         return _loc4_;
      }
      
      public static function copy(param1:List) : List
      {
         return Lambda.list(param1);
      }
      
      public static function flatten(param1:List) : List
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:* = null;
         var _loc8_:* = null as Object;
         var _loc2_:List = new List();
         var _loc3_:int = 0;
         var _loc4_:int = param1.length;
         while(_loc3_ < _loc4_)
         {
            _loc5_ = _loc3_++;
            _loc6_ = _loc5_;
            _loc7_ = param1.iterator();
            while(true)
            {
               _loc6_--;
               if(_loc6_ <= -1)
               {
                  break;
               }
               _loc7_.next();
            }
            _loc7_ = _loc7_.next().iterator();
            while(_loc7_.§\n\x1cT[\x02§())
            {
               _loc8_ = _loc7_.next();
               _loc2_.add(_loc8_);
            }
            _loc2_;
         }
         return _loc2_;
      }
      
      public static function append(param1:List, param2:Object) : List
      {
         var _loc4_:* = null as Object;
         var _loc3_:* = param2.iterator();
         while(_loc3_.§\n\x1cT[\x02§())
         {
            _loc4_ = _loc3_.next();
            param1.add(_loc4_);
         }
         return param1;
      }
      
      public static function prepend(param1:List, param2:Object) : List
      {
         var _loc5_:* = null as Object;
         var _loc3_:Array = Lambda.array(param2);
         _loc3_.reverse();
         var _loc4_:int = 0;
         while(_loc4_ < int(_loc3_.length))
         {
            _loc5_ = _loc3_[_loc4_];
            _loc4_++;
            param1.push(_loc5_);
            param1;
         }
         return param1;
      }
      
      public static function reverse(param1:List) : List
      {
         var _loc2_:Array = [];
         while(param1.length > 0)
         {
            _loc2_.unshift(param1.pop());
            _loc2_;
         }
         while(int(_loc2_.length) > 0)
         {
            param1.push(_loc2_.pop());
            param1;
         }
         return param1;
      }
      
      public static function §7\x03\b\n§(param1:List, param2:Object = undefined) : List
      {
         var _loc6_:int = 0;
         var _loc3_:Array = Lambda.array(param1);
         ArrayStd.§7\x03\b\n§(_loc3_,param2);
         param1.clear();
         var _loc4_:int = 0;
         var _loc5_:int = int(_loc3_.length);
         while(_loc4_ < _loc5_)
         {
            _loc6_ = _loc4_++;
            param1.add(_loc3_[_loc6_]);
            param1;
         }
         _loc3_ = null;
         return param1;
      }
      
      public static function slice(param1:List, param2:int, param3:Object = undefined) : List
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc4_:List = new List();
         if(param3 == null)
         {
            param3 = param1.length;
         }
         var _loc5_:int = param2;
         while(_loc5_ < int(param3))
         {
            _loc6_ = _loc5_++;
            _loc7_ = _loc6_;
            _loc8_ = param1.iterator();
            while(true)
            {
               _loc7_--;
               if(_loc7_ <= -1)
               {
                  break;
               }
               _loc8_.next();
            }
            _loc4_.add(_loc8_.next());
            _loc4_;
         }
         return _loc4_;
      }
      
      public static function splice(param1:List, param2:int, param3:int) : List
      {
         var _loc8_:* = null as Object;
         var _loc4_:List = new List();
         var _loc5_:List = Lambda.list(param1);
         param1.clear();
         var _loc6_:int = 0;
         var _loc7_:* = _loc5_.iterator();
         while(_loc7_.§\n\x1cT[\x02§())
         {
            _loc8_ = _loc7_.next();
            if(_loc6_ < param2)
            {
               param1.add(_loc8_);
               param1;
            }
            else if(_loc6_ >= param2 + param3)
            {
               param1.add(_loc8_);
               param1;
            }
            else
            {
               _loc4_.add(_loc8_);
               _loc4_;
            }
            _loc6_++;
         }
         return _loc4_;
      }
      
      public static function stripNull(param1:List) : List
      {
         while(param1.remove(null))
         {
         }
         return param1;
      }
      
      public static function getRandom(param1:List, param2:Object = undefined) : Object
      {
         var _loc3_:* = param2 != null ? param2 : Std.random;
         var _loc4_:int = int(_loc3_(param1.length));
         var _loc5_:int = _loc4_;
         var _loc6_:* = param1.iterator();
         while(true)
         {
            _loc5_--;
            if(_loc5_ <= -1)
            {
               break;
            }
            _loc6_.next();
         }
         return _loc6_.next();
      }
      
      public static function §s}\nE§(param1:List, param2:Function) : List
      {
         var _loc5_:* = null as Object;
         var _loc3_:Array = Lambda.array(param1);
         _loc3_ = ArrayStd.§s}\nE§(_loc3_,param2);
         param1.clear();
         var _loc4_:int = 0;
         while(_loc4_ < int(_loc3_.length))
         {
            _loc5_ = _loc3_[_loc4_];
            _loc4_++;
            param1.add(_loc5_);
            param1;
         }
         return param1;
      }
   }
}
