package mt
{
   import flash.Boot;
   
   public class Rand
   {
       
      
      public var seed:Number;
      
      public function Rand(param1:int = 0)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         seed = (param1 < 0 ? -param1 : param1) + 131;
      }
      
      public function sign() : int
      {
         return int(((int(seed = Number(seed * 16807 % 2147483647))) & 1073741823) % 2) * 2 - 1;
      }
      
      public function range(param1:Number, param2:Number, param3:Object = undefined) : Number
      {
         var _loc4_:Number = NaN;
         if(param3 == null)
         {
            param3 = false;
         }
         return (param1 + int(((int(seed = Number(seed * 16807 % 2147483647))) & 1073741823) % 10007) / 10007 * (param2 - param1)) * (!!param3 ? int(((int(seed = Number(seed * 16807 % 2147483647))) & 1073741823) % 2) * 2 - 1 : 1);
      }
      
      public function random(param1:int) : int
      {
         return int(((int(seed = Number(seed * 16807 % 2147483647))) & 1073741823) % param1);
      }
      
      public function §\x0f#\b\x02§() : Number
      {
         return int(((int(seed = Number(seed * 16807 % 2147483647))) & 1073741823) % 10007) / 10007;
      }
      
      public function irange(param1:int, param2:int, param3:Object = undefined) : int
      {
         var _loc4_:Number = NaN;
         if(param3 == null)
         {
            param3 = false;
         }
         return (param1 + int(((int(seed = Number(seed * 16807 % 2147483647))) & 1073741823) % (param2 - param1 + 1))) * (!!param3 ? int(((int(seed = Number(seed * 16807 % 2147483647))) & 1073741823) % 2) * 2 - 1 : 1);
      }
      
      public function _int() : int
      {
         return (int(seed = Number(seed * 16807 % 2147483647))) & 1073741823;
      }
      
      public function initSeed(param1:int, param2:Object = undefined) : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         if(param2 == null)
         {
            param2 = 5;
         }
         var _loc3_:int = 0;
         while(_loc3_ < int(param2))
         {
            _loc4_ = _loc3_++;
            param1 = param1 ^ param1 << 7 & 727393536;
            param1 = param1 ^ param1 << 15 & 462094336;
            param1 = param1 ^ param1 >>> 16;
            param1 = param1 & 1073741823;
            _loc5_ = 5381;
            _loc5_ = (_loc5_ << 5) + _loc5_ + (param1 & 255);
            _loc5_ = (_loc5_ << 5) + _loc5_ + (param1 >> 8 & 255);
            _loc5_ = (_loc5_ << 5) + _loc5_ + (param1 >> 16 & 255);
            _loc5_ = (_loc5_ << 5) + _loc5_ + (param1 >> 24);
            param1 = _loc5_ & 1073741823;
         }
         seed = (param1 & 536870911) + 131;
      }
      
      public function getSeed() : int
      {
         return int(seed) - 131;
      }
      
      public function clone() : Rand
      {
         var _loc1_:Rand = new Rand(0);
         _loc1_.seed = seed;
         return _loc1_;
      }
      
      public function addSeed(param1:int) : void
      {
         seed = int((seed + param1) % 2147483647) & 1073741823;
         if(seed == 0)
         {
            seed = param1 + 1;
         }
      }
   }
}
