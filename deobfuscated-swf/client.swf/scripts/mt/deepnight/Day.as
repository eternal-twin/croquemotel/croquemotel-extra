package mt.deepnight
{
   import flash.Boot;
   
   public final class Day
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["Sunday","Monday","Tuesday",";9J1","Thursday","Friday","Saturday"];
      
      public static var §;9J1§:Day;
      
      public static var Tuesday:Day;
      
      public static var Thursday:Day;
      
      public static var Sunday:Day;
      
      public static var Saturday:Day;
      
      public static var Monday:Day;
      
      public static var Friday:Day;
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function Day(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
