package
{
   import flash.Boot;
   
   public class _Room
   {
      
      public static var §\x1cZu_\x01§:int = 5;
       
      
      public var _underConstruction:Date;
      
      public var _type:_TypeRoom;
      
      public var _serviceEnd:Date;
      
      public var _life:int;
      
      public var _level:int;
      
      public var _itemToTake:_Item;
      
      public var _id:int;
      
      public var _floor:int;
      
      public var _equipments:List;
      
      public var _effectSpread:List;
      
      public var _clientId:Object;
      
      public function _Room()
      {
      }
      
      public static function §0B;!\x01§(param1:int, param2:_TypeRoom) : List
      {
         var _loc3_:List = new List();
         _loc3_.add(_TypeRoom._TR_BEDROOM);
         _loc3_.add(_TypeRoom._TR_RESTAURANT);
         _loc3_.add(_TypeRoom._TR_BIN);
         _loc3_.add(_TypeRoom._TR_DISCO);
         _loc3_.add(_TypeRoom._TR_FURNACE);
         _loc3_.add(_TypeRoom._TR_POOL);
         _loc3_.add(_TypeRoom._TR_SERV_WASH);
         _loc3_.add(_TypeRoom._TR_SERV_SHOE);
         _loc3_.add(_TypeRoom._TR_SERV_ALCOOL);
         _loc3_.add(_TypeRoom._TR_SERV_FRIDGE);
         _loc3_.add(_TypeRoom._TR_LAB);
         if(param1 == 0)
         {
            _loc3_.remove(_TypeRoom._TR_BEDROOM);
         }
         switch(int(param2.index))
         {
            case 0:
               _loc3_ = new List();
               break;
            case 3:
               _loc3_ = new List();
         }
         _loc3_.remove(param2);
         return _loc3_;
      }
      
      public static function §w&;c\x02§(param1:int, param2:int) : int
      {
         return param1 * 100 + (param2 + 1);
      }
      
      public static function §;#\fS\x01§(param1:_TypeRoom) : Boolean
      {
         return int(Std.string(param1).toLowerCase().indexOf("serv_")) >= 0;
      }
      
      public static function §fHRr\x02§(param1:_TypeRoom) : Boolean
      {
         switch(int(param1.index))
         {
            case 0:
            case 1:
            case 2:
            case 3:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
               break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
         }
         false;
         true;
         return Boolean(null);
      }
      
      public static function §;4\r\x13\x02§(param1:_TypeRoom) : int
      {
         return param1 == _TypeRoom._TR_BEDROOM ? Const.§\x19'0Z\x02§ : (param1 == _TypeRoom._TR_LAB ? Const.§$D}(\x01§ : (Boolean(_Room.§;#\fS\x01§(param1)) ? Const.§\x01\r\tF\x02§ : 666));
      }
      
      public static function §\x1c;m\x07\x03§(param1:_ServiceType) : _TypeRoom
      {
         switch(int(param1.index))
         {
            case 0:
               break;
            case 1:
               break;
            case 2:
               break;
            case 3:
         }
         _TypeRoom._TR_SERV_WASH;
         _TypeRoom._TR_SERV_SHOE;
         _TypeRoom._TR_SERV_FRIDGE;
         _TypeRoom._TR_SERV_ALCOOL;
         return null;
      }
      
      public static function §\f\x03[#\x01§(param1:_TypeRoom) : Object
      {
         var _loc2_:String = T.§\x0e\x07;`\x01§(Std.string(param1).substr(1));
         if(int(_loc2_.indexOf("J")) < 0)
         {
            _loc2_ = _loc2_ + "||";
         }
         _loc2_ = StringTools.replace(_loc2_,"Hc","J");
         var _loc3_:Array = _loc2_.split("J");
         return {
            "_name":StringTools.trim(_loc3_[0]),
            "_ambiant":StringTools.trim(_loc3_[1]),
            "_rule":StringTools.trim(_loc3_[2])
         };
      }
      
      public function §;$l\x16§() : void
      {
         _equipments = new List();
      }
      
      public function §k\x04u\f\x03§(param1:_Item) : void
      {
         if(!§\t\x10;F\x03§(param1))
         {
            Boot.lastError = new Error();
            throw "not found";
         }
         _equipments.remove(param1);
      }
      
      public function §`S|g\x02§() : Boolean
      {
         return _type == _TypeRoom._TR_BEDROOM && _life < _Room.§\x1cZu_\x01§;
      }
      
      public function §/B(=§(param1:_Item) : void
      {
         if(§\t\x10;F\x03§(param1))
         {
            Boot.lastError = new Error();
            throw "already installed";
         }
         _equipments.add(param1);
      }
      
      public function §\x1fGmm§() : Boolean
      {
         return _equipments.length < 2;
      }
      
      public function §\t\x10;F\x03§(param1:_Item = undefined) : Boolean
      {
         var _loc2_:* = null;
         var _loc3_:* = null as _Item;
         if(param1 == null)
         {
            return _equipments.length > 0;
         }
         _loc2_ = _equipments.iterator();
         while(_loc2_.§\n\x1cT[\x02§())
         {
            _loc3_ = _loc2_.next();
            if(_loc3_ == param1)
            {
               return true;
            }
         }
         return false;
      }
   }
}
