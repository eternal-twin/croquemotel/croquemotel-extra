package
{
   import _T.§\x13R\x1e(\x03§;
   import _T.§)\x13#$\x03§;
   import flash.Boot;
   import haxe.Log;
   import haxe.Resource;
   import haxe.xml.Fast;
   
   public class T
   {
      
      public static var init__:Boolean;
      
      public static var ALL:Hash;
      
      public static var get:§\x13R\x1e(\x03§;
      
      public static var format:§)\x13#$\x03§;
       
      
      public function T()
      {
      }
      
      public static function init() : void
      {
         T.get = T.parse();
      }
      
      public static function parse() : §\x13R\x1e(\x03§
      {
         var _loc2_:* = null as String;
         var _loc3_:* = null as Hash;
         var _loc4_:* = null as Xml;
         var _loc5_:* = null as Fast;
         var _loc6_:* = null;
         var _loc7_:* = null as Fast;
         try
         {
            _loc2_ = Resource.getString(Game.LANG + ".lang.xml");
            _loc3_ = new Hash();
            _loc4_ = Xml.parse(_loc2_);
            _loc5_ = new Fast(_loc4_.firstChild());
            _loc6_ = _loc5_.nodes.resolve("t").iterator();
            while(_loc6_.§\n\x1cT[\x02§())
            {
               _loc7_ = _loc6_.next();
               _loc3_.set(_loc7_.att.resolve("id"),T.§5tgt§(_loc7_.get_innerHTML()));
            }
            T.ALL = _loc3_;
            return new §\x13R\x1e(\x03§(T.ALL.get);
         }
         catch(_loc_e_:*)
         {
            Log.trace(_loc6_,{
               "fileName":"T.hx",
               "lineNumber":37,
               "className":"T",
               "methodName":"parse"
            });
            Boot.lastError = new Error();
            throw "FAILED";
         }
      }
      
      public static function §5tgt§(param1:String) : String
      {
         return T.§XK;\x03§(param1,"*","<strong><span class=\'strong\'>","</span></strong>");
      }
      
      public static function §XK;\x03§(param1:String, param2:String, param3:String, param4:String) : String
      {
         var _loc9_:* = null as String;
         var _loc5_:Array = param1.split(param2);
         if(int(_loc5_.length) == 0)
         {
            return param1;
         }
         param1 = "";
         var _loc6_:Boolean = true;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         while(_loc8_ < int(_loc5_.length))
         {
            _loc9_ = _loc5_[_loc8_];
            _loc8_++;
            if(_loc7_ == int(_loc5_.length) - 1)
            {
               param1 = param1 + _loc9_;
            }
            else if(int(_loc7_ % 2) == 0)
            {
               param1 = param1 + (_loc9_ + param3);
            }
            else
            {
               param1 = param1 + (_loc9_ + param4);
            }
            _loc7_++;
         }
         return param1;
      }
      
      public static function _format(param1:String) : Function
      {
         var k:String = param1;
         return function(param1:*):String
         {
            var _loc5_:* = null as String;
            var _loc2_:String = Boolean(T.ALL.§=Vr§(k)) ? T.ALL.get(k) : "#" + k + "#";
            var _loc3_:int = 0;
            var _loc4_:Array = Reflect.fields(param1);
            while(_loc3_ < int(_loc4_.length))
            {
               _loc5_ = _loc4_[_loc3_];
               _loc3_++;
               _loc2_ = StringTools.replace(_loc2_,"::" + _loc5_.substr(1) + "::",Std.string(Reflect.field(param1,_loc5_)));
            }
            return _loc2_;
         };
      }
      
      public static function §\x0e\x07;`\x01§(param1:String) : String
      {
         return Boolean(T.ALL.§=Vr§(param1)) ? T.ALL.get(param1) : "#" + param1 + "#";
      }
      
      public static function §Z\x19nM\x02§(param1:String, param2:*) : String
      {
         return Boolean(T.ALL.§=Vr§(param1)) ? T._format(param1)(param2) : "#" + param1 + "#";
      }
      
      public static function §s6\x03`\x01§(param1:_MonsterFamily) : String
      {
         return T.§\x0e\x07;`\x01§("Rule_" + Std.string(param1).substr(4));
      }
      
      public static function §9\x18V$§(param1:_Item) : Object
      {
         var _loc2_:String = T.§\x0e\x07;`\x01§(Std.string(param1).substr(1));
         if(int(_loc2_.indexOf("J")) < 0)
         {
            _loc2_ = _loc2_ + "||";
         }
         _loc2_ = StringTools.replace(_loc2_,"Hc","J");
         var _loc3_:Array = _loc2_.split("J");
         return {
            "_name":StringTools.trim(_loc3_[0]),
            "_ambiant":StringTools.trim(_loc3_[1]),
            "_rule":StringTools.trim(_loc3_[2])
         };
      }
   }
}
