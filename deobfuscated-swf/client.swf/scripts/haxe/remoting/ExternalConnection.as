package haxe.remoting
{
   import flash.Boot;
   import flash.external.ExternalInterface;
   import haxe.Serializer;
   import haxe.Unserializer;
   
   public dynamic class ExternalConnection implements Connection
   {
      
      public static var init__:Boolean;
      
      public static var connections:Hash;
       
      
      public var __path:Array;
      
      public var __data:Object;
      
      public function ExternalConnection(param1:Object = undefined, param2:Array = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         __data = param1;
         __path = param2;
      }
      
      public static function escapeString(param1:String) : String
      {
         return param1.split("\\").join("\\\\");
      }
      
      public static function doCall(param1:String, param2:String, param3:String) : String
      {
         var _loc5_:* = null as ExternalConnection;
         var _loc6_:* = null as Array;
         var _loc7_:* = null;
         var _loc8_:* = null as Serializer;
         try
         {
            _loc5_ = ExternalConnection.connections.get(param1);
            if(_loc5_ == null)
            {
               Boot.lastError = new Error();
               throw "Unknown connection : " + param1;
            }
            if(_loc5_.__data.ctx == null)
            {
               Boot.lastError = new Error();
               throw "No context shared for the connection " + param1;
            }
            _loc6_ = new Unserializer(param3).unserialize();
            _loc7_ = _loc5_.__data.ctx.call(param2.split("."),_loc6_);
            _loc8_ = new Serializer();
            _loc8_.serialize(_loc7_);
            return ExternalConnection.escapeString(_loc8_.toString());
         }
         catch(_loc_e_:*)
         {
            _loc8_ = new Serializer();
            _loc8_.serializeException(_loc7_);
            return _loc8_.toString();
         }
      }
      
      public static function jsConnect(param1:String, param2:§\x19A-\x03§ = undefined) : ExternalConnection
      {
         var _loc4_:* = null;
         if(!ExternalInterface.available)
         {
            Boot.lastError = new Error();
            throw "External Interface not available";
         }
         try
         {
            ExternalInterface.addCallback("externalRemotingCall",ExternalConnection.doCall);
         }
         catch(_loc_e_:*)
         {
            var _loc5_:ExternalConnection = new ExternalConnection({
               "name":param1,
               "ctx":param2
            },[]);
            ExternalConnection.connections.set(param1,_loc5_);
            return _loc5_;
         }
      }
      
      public function resolve(param1:String) : Connection
      {
         var _loc2_:ExternalConnection = new ExternalConnection(__data,__path.copy());
         _loc2_.__path.push(param1);
         return _loc2_;
      }
      
      public function close() : void
      {
         ExternalConnection.connections.remove(__data.name);
      }
      
      public function call(param1:Array) : *
      {
         var _loc2_:Serializer = new Serializer();
         _loc2_.serialize(param1);
         var _loc3_:String = ExternalConnection.escapeString(_loc2_.toString());
         var _loc4_:String = null;
         _loc4_ = ExternalInterface.call("haxe.remoting.ExternalConnection.doCall",__data.name,__path.join("."),_loc3_);
         if(_loc4_ == null)
         {
            Boot.lastError = new Error();
            throw "Call failure : ExternalConnection is not " + "compiled in JS";
         }
         return new Unserializer(_loc4_).unserialize();
      }
   }
}
