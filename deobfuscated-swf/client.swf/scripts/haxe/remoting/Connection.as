package haxe.remoting
{
   public interface Connection
   {
       
      
      function resolve(param1:String) : Connection;
      
      function call(param1:Array) : *;
   }
}
