package haxe.xml
{
   import flash.Boot;
   import haxe.xml._Fast.AttribAccess;
   import haxe.xml._Fast.HasNodeAccess;
   import haxe.xml._Fast.NodeAccess;
   import haxe.xml._Fast.NodeListAccess;
   import haxe.xml._Fast.§w6p§;
   
   public class Fast
   {
       
      
      public var x:Xml;
      
      public var nodes:NodeListAccess;
      
      public var node:NodeAccess;
      
      public var name:String;
      
      public var innerHTML:String;
      
      public var innerData:String;
      
      public var §\x12\n&\x02\x01§:HasNodeAccess;
      
      public var has:§w6p§;
      
      public var elements:Object;
      
      public var att:AttribAccess;
      
      public function Fast(param1:Xml = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         if(param1.nodeType != Xml.Document && param1.nodeType != Xml.Element)
         {
            Boot.lastError = new Error();
            throw "Invalid nodeType " + Std.string(param1.nodeType);
         }
         x = param1;
         node = new NodeAccess(param1);
         nodes = new NodeListAccess(param1);
         att = new AttribAccess(param1);
         has = new §w6p§(param1);
         §\x12\n&\x02\x01§ = new HasNodeAccess(param1);
      }
      
      public function get_name() : String
      {
         return x.nodeType == Xml.Document ? "Document" : x.§\\p\x19g§();
      }
      
      public function get_innerHTML() : String
      {
         var _loc3_:* = null as Xml;
         var _loc1_:StringBuf = new StringBuf();
         var _loc2_:* = x.iterator();
         while(_loc2_.§\n\x1cT[\x02§())
         {
            _loc3_ = _loc2_.next();
            _loc1_.b = _loc1_.b + Std.string(_loc3_.toString());
         }
         return _loc1_.b;
      }
      
      public function get_innerData() : String
      {
         var _loc4_:* = null as Xml;
         var _loc1_:* = x.iterator();
         if(!_loc1_.§\n\x1cT[\x02§())
         {
            Boot.lastError = new Error();
            throw get_name() + " does not have data";
         }
         var _loc2_:Xml = _loc1_.next();
         var _loc3_:Xml = _loc1_.next();
         if(_loc3_ != null)
         {
            if(_loc2_.nodeType == Xml.PCData && _loc3_.nodeType == Xml.CData && StringTools.trim(_loc2_.§\x07\x15~U§()) == "")
            {
               _loc4_ = _loc1_.next();
               if(_loc4_ == null || _loc4_.nodeType == Xml.PCData && StringTools.trim(_loc4_.§\x07\x15~U§()) == "" && _loc1_.next() == null)
               {
                  return _loc3_.§\x07\x15~U§();
               }
            }
            Boot.lastError = new Error();
            throw get_name() + " does not only have data";
         }
         if(_loc2_.nodeType != Xml.PCData && _loc2_.nodeType != Xml.CData)
         {
            Boot.lastError = new Error();
            throw get_name() + " does not have data";
         }
         return _loc2_.§\x07\x15~U§();
      }
      
      public function get_elements() : Object
      {
         var ez:Object = x.elements();
         return {
            "\n\x1cT[\x02":ez.§\n\x1cT[\x02§,
            "next":function():Fast
            {
               var _loc1_:Xml = ez.next();
               if(_loc1_ == null)
               {
                  return null;
               }
               return new Fast(_loc1_);
            }
         };
      }
   }
}
