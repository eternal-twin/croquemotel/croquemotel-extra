package haxe.xml._Fast
{
   import flash.Boot;
   
   public dynamic class AttribAccess
   {
       
      
      public var __x:Xml;
      
      public function AttribAccess(param1:Xml = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         __x = param1;
      }
      
      public function resolve(param1:String) : String
      {
         if(__x.nodeType == Xml.Document)
         {
            Boot.lastError = new Error();
            throw "Cannot access document attribute " + param1;
         }
         var _loc2_:String = __x.get(param1);
         if(_loc2_ == null)
         {
            Boot.lastError = new Error();
            throw __x.§\\p\x19g§() + " is missing attribute " + param1;
         }
         return _loc2_;
      }
   }
}
