package haxe.xml._Fast
{
   import flash.Boot;
   
   public dynamic class §w6p§
   {
       
      
      public var __x:Xml;
      
      public function §w6p§(param1:Xml = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         __x = param1;
      }
      
      public function resolve(param1:String) : Boolean
      {
         if(__x.nodeType == Xml.Document)
         {
            Boot.lastError = new Error();
            throw "Cannot access document attribute " + param1;
         }
         return Boolean(__x.§=Vr§(param1));
      }
   }
}
