package haxe
{
   import flash.Boot;
   import flash.events.HTTPStatusEvent;
   import flash.events.IOErrorEvent;
   import flash.events.SecurityErrorEvent;
   import flash.net.URLLoader;
   import flash.net.URLRequest;
   import flash.net.URLRequestHeader;
   import flash.net.URLVariables;
   
   public class Http
   {
       
      
      public var url:String;
      
      public var postData:String;
      
      public var params:Hash;
      
      public var onStatus:Function;
      
      public var onError:Function;
      
      public var onData:Function;
      
      public var headers:Hash;
      
      public function Http(param1:String = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         if(!onData)
         {
            onData = function(param1:String):void
            {
            };
         }
         if(!onError)
         {
            onError = function(param1:String):void
            {
            };
         }
         if(!onStatus)
         {
            onStatus = function(param1:int):void
            {
            };
         }
         url = param1;
         headers = new Hash();
         params = new Hash();
      }
      
      public function setPostData(param1:String) : void
      {
         postData = param1;
      }
      
      public function setParameter(param1:String, param2:String) : void
      {
         params.set(param1,param2);
      }
      
      public function §Ncn]\x03§(param1:String, param2:String) : void
      {
         headers.set(param1,param2);
      }
      
      public function request(param1:Boolean) : void
      {
         var _loc5_:* = null;
         var _loc6_:* = null as String;
         var _loc7_:* = null as Array;
         var _loc9_:* = null as String;
         var me:Http = this;
         var loader:URLLoader = new URLLoader();
         loader.addEventListener("complete",function(param1:*):void
         {
            me.onData(loader.data);
         });
         loader.addEventListener("httpStatus",function(param1:HTTPStatusEvent):void
         {
            if(param1.status != 0)
            {
               me.onStatus(param1.status);
            }
         });
         loader.addEventListener("ioError",function(param1:IOErrorEvent):void
         {
            me.onError(param1.text);
         });
         loader.addEventListener("securityError",function(param1:SecurityErrorEvent):void
         {
            me.onError(param1.text);
         });
         var _loc3_:Boolean = false;
         var _loc4_:URLVariables = new URLVariables();
         _loc5_ = params.keys();
         while(_loc5_.§\n\x1cT[\x02§())
         {
            _loc6_ = _loc5_.next();
            _loc3_ = true;
            _loc4_[_loc6_] = params.get(_loc6_);
         }
         _loc6_ = url;
         if(!!_loc3_ && !param1)
         {
            _loc7_ = url.split("?");
            if(int(_loc7_.length) > 1)
            {
               _loc6_ = _loc7_.shift();
               _loc4_.decode(_loc7_.join("?"));
            }
         }
         _loc7_ = _loc6_.split("xxx");
         var _loc8_:URLRequest = new URLRequest(_loc6_);
         _loc5_ = headers.keys();
         while(_loc5_.§\n\x1cT[\x02§())
         {
            _loc9_ = _loc5_.next();
            _loc8_.requestHeaders.push(new URLRequestHeader(_loc9_,headers.get(_loc9_)));
         }
         if(postData != null)
         {
            _loc8_.data = postData;
            _loc8_.method = "POST";
         }
         else
         {
            _loc8_.data = _loc4_;
            _loc8_.method = !!param1 ? "POST" : "GET";
         }
         try
         {
            loader.load(_loc8_);
         }
         catch(_loc_e_:*)
         {
            onError("Exception: " + Std.string(_loc5_));
            return;
         }
      }
   }
}
